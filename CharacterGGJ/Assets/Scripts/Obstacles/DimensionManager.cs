﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//controlls the switching of components that affect the collisions and view of the object
//attach to object and have player control change the dimension
public class DimensionManager : MonoBehaviour {

    public static DimensionManager dimensionInstance;

    public int currentDimension;

    void Awake()
    {
        if (dimensionInstance == null)
        {
            dimensionInstance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ChangeDimension()
    {

        currentDimension = (currentDimension + 1) % 2;

    }

    public int getDimension()
    {
        return currentDimension;
    }
}
