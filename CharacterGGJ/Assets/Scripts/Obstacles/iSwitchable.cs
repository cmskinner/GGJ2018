﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface iSwitchable {

    void Switch();

    void Reset();

}
