﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//this script should be enabled or disabled from basic script to prevent constant updating

[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class Dimensional : MonoBehaviour {

    public bool isDimensional = false;
    //this object will only be in effect when active dimension matches current dimension
    public int activeDimension = 0;

    private bool isProjectile = false;

    private Collider2D collider;
    private SpriteRenderer sprite;

    void Awake()
    {



        if (sprite == null)
        {
            collider = GetComponent<BoxCollider2D>();
            sprite = GetComponent<SpriteRenderer>();
        }

        if(GetComponent<Projectile>() != null)
        {
            isProjectile = true;
        }


    }

	// Update is called once per frame
	void Update () {

        if (DimensionManager.dimensionInstance.currentDimension == activeDimension)
        {

            collider.enabled = true;
            sprite.enabled = true;
            //sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 1);
        }
        else
        {
            if (isProjectile)
            {
                collider.enabled = false;
            }

            sprite.enabled = false;
            //sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 0);
        }

    }

}
