﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Dimensional))]
[RequireComponent(typeof(BoxCollider2D))]
public class Platform : MonoBehaviour {

    public BoxCollider2D collider2D;

    void Awake()
    {
        collider2D = GetComponent<BoxCollider2D>();
    }



}
