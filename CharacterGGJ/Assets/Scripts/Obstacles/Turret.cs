﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//the circle collider serves as a trigger to begin tracking player movement
//box serves as a way to destroy the turret
//Dimensional can allow for turret to exist in one or both dimensions
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Dimensional))]
public class Turret : MonoBehaviour, iDestroyable {


    bool playerIsVisible;

    public float targetRange = 10f;
    //the delay in milliseconds between shots
    public float fireRate;


    private float timeDiff = 500f;

    private CircleCollider2D circleCollider;
    private BoxCollider2D boxCollider;

    public void destroy()
    {
        Destroy(gameObject);
    }

    void Awake()
    {

        circleCollider = GetComponent<CircleCollider2D>();
        boxCollider = GetComponent<BoxCollider2D>();

        circleCollider.isTrigger = true;
        circleCollider.radius = targetRange;

    }
	// Update is called once per frame
	void Update () {

        if (playerIsVisible)
        {

        }

	}

    //perform some action based on player entering
    void OnTriggerEnter(Collider player)
    {
        if(player.gameObject.GetComponent<Player>() != null)
        {

        }

    }

    //track player movement
    void OnTriggerStay(Collider player)
    {
        timeDiff += Time.deltaTime;
        if(timeDiff > fireRate)
        {
            Fire();
            timeDiff = timeDiff % fireRate;
        }

    }

    //handle turret fire method
    private void Fire()
    {
        //create projectile then fire
    }

}
