﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayer{

    void KillPlayer();

    Vector2 PlayerPosition();

}
