﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour, IPlayer{

    [HideInInspector]
    public bool facingLeft = true;

    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 50f;
    public float wallBounceForce = 200f;

    private bool jump = false;
    private bool hasJumped = false;
    private Transform groundCheck;
    private bool grounded;
    private Transform wallCheckL;
    private Transform wallCheckR;
    private bool walledL;
    private bool walledR;

    private float currentTime;
    private float lastTime;


    private Vector2 currentPosition;
    private Vector2 lastPostition;

    private Animator anim;

    public void KillPlayer()
    {
        Destroy(this);
    }

    public Vector2 PlayerPosition()
    {
        return this.transform.position;
    }

    public void Awake()
    {
        groundCheck = transform.Find("groundCheck");
        wallCheckL = transform.Find("wallCheckL");
        wallCheckR = transform.Find("wallCheckR");

        anim = GetComponent<Animator>();
    }

    public void Update()
    {
        grounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        walledL = Physics2D.Linecast(transform.position, wallCheckL.position, 1 << LayerMask.NameToLayer("Ground"));
        walledR = Physics2D.Linecast(transform.position, wallCheckR.position, 1 << LayerMask.NameToLayer("Ground"));

        if (Input.GetButtonDown("Jump") && (grounded || walledL || walledR)) //walledR is used for back detection to jump off a wall while not facing it
        {
            jump = true;
            hasJumped = true;
        }

        if (jump)
        {
            anim.SetTrigger("Jump");
            if (walledL)
            {
                GetComponent<Rigidbody2D>().AddForce(Vector2.right * wallBounceForce);
            }
            GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpForce;
            jump = false;
        }
        
    }

    public void FixedUpdate()
    {

        float h = Input.GetAxis("Horizontal");

        anim.SetFloat("Speed", Mathf.Abs(h));
        
        if (h < 0 && !facingLeft)
            // ... flip the player.
            Flip();
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (h > 0 && facingLeft)
            // ... flip the player.
            Flip();

        
        if (walledL)
        {
            // Create a slight wall hold
        }
        else
        {
            if (h * GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
                GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);

            if (Mathf.Abs(GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
                GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        
        if (grounded)
        {
            anim.SetBool("GroundedBool", true);
        }
        else
        {
            anim.SetBool("GroundedBool", false);
        }
        if (walledL || walledR)
        {
            anim.SetBool("WalledBool", true);
        }
        else
        {
            anim.SetBool("WalledBool", false);
        }
    }

    void Flip()
    {
        facingLeft = !facingLeft;
        
        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

}
