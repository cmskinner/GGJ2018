﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Vector3 offset;          // The offset at which the Health Bar follows the player.

    private Transform player;       // Reference to the player.


    void Awake()
    {
        // Setting up the reference.
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }

    void Update()
    {
        // Set the position to the player's position with the offset.
        try
        {

            transform.position = player.position + offset;
        }
        catch (MissingReferenceException ex)
        {
            // ignoring because it only happens when the character is dead
            // and is trying to respawn.
        }
    }
}
